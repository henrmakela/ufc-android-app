package com.example.henri.ufcfighters;

import java.io.Serializable;

/**
 * Created by Henri on 7.8.2017.
 */

public class Event implements Serializable{


    private Integer id;


    private String event_date;

    private String secondaryFeatureImage;

    private String ticketImage;

    private String eventTimeZoneText;

    private String shortDescription;

    private String eventDategmt;

    private String endEventDategmt;

    private String ticketurl;

    private Object ticketSellerName;

    private String base_title;

    private String title_tag_line;

    private String twitterHashtag;

    private String ticketGeneralSaleDate;

    private Object statid;

    private String feature_image;

    private String eventTimeText;

    private String ticketGeneralSaleText;

    private String subtitle;

    private String eventStatus;

    private String isppvevent;

    private String cornerAudioAvailable;

    private Object cornerAudioBlueStreamUrl;

    private Object cornerAudioRedStreamUrl;

    private String lastModified;

    private String urlName;

    private String created;

    private Object trailerUrl;

    private String arena;

    private String location;

    private String fm_fnt_feed_url;

    private Integer main_event_fighter1_id;

    private Integer main_event_fighter2_id;




    public String getFm_fnt_feed_url() {
        return fm_fnt_feed_url;
    }



    public Integer getMain_event_fighter1_id() {
        return main_event_fighter1_id;
    }



    public Integer getMain_event_fighter2_id() {
        return main_event_fighter2_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEventDate() {

        /*


*/



        return event_date;
    }



    public String getSecondaryFeatureImage() {
        return secondaryFeatureImage;
    }

    public void setSecondaryFeatureImage(String secondaryFeatureImage) {
        this.secondaryFeatureImage = secondaryFeatureImage;
    }

    public String getTicketImage() {
        return ticketImage;
    }

    public void setTicketImage(String ticketImage) {
        this.ticketImage = ticketImage;
    }

    public String getEventTimeZoneText() {
        return eventTimeZoneText;
    }

    public void setEventTimeZoneText(String eventTimeZoneText) {
        this.eventTimeZoneText = eventTimeZoneText;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getEventDategmt() {
        return eventDategmt;
    }

    public void setEventDategmt(String eventDategmt) {
        this.eventDategmt = eventDategmt;
    }

    public String getEndEventDategmt() {
        return endEventDategmt;
    }

    public void setEndEventDategmt(String endEventDategmt) {
        this.endEventDategmt = endEventDategmt;
    }

    public String getTicketurl() {
        return ticketurl;
    }

    public void setTicketurl(String ticketurl) {
        this.ticketurl = ticketurl;
    }

    public Object getTicketSellerName() {
        return ticketSellerName;
    }

    public void setTicketSellerName(Object ticketSellerName) {
        this.ticketSellerName = ticketSellerName;
    }

    public String getBaseTitle() {
        return base_title;
    }



    public String getTitleTagLine() {
        return title_tag_line;
    }


    public String getTwitterHashtag() {
        return twitterHashtag;
    }

    public void setTwitterHashtag(String twitterHashtag) {
        this.twitterHashtag = twitterHashtag;
    }

    public String getTicketGeneralSaleDate() {
        return ticketGeneralSaleDate;
    }

    public void setTicketGeneralSaleDate(String ticketGeneralSaleDate) {
        this.ticketGeneralSaleDate = ticketGeneralSaleDate;
    }

    public Object getStatid() {
        return statid;
    }

    public void setStatid(Object statid) {
        this.statid = statid;
    }

    public String getFeatureImage() {
        return feature_image;
    }



    public String getEventTimeText() {
        return eventTimeText;
    }

    public void setEventTimeText(String eventTimeText) {
        this.eventTimeText = eventTimeText;
    }

    public String getTicketGeneralSaleText() {
        return ticketGeneralSaleText;
    }

    public void setTicketGeneralSaleText(String ticketGeneralSaleText) {
        this.ticketGeneralSaleText = ticketGeneralSaleText;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public String getIsppvevent() {
        return isppvevent;
    }

    public void setIsppvevent(String isppvevent) {
        this.isppvevent = isppvevent;
    }

    public String getCornerAudioAvailable() {
        return cornerAudioAvailable;
    }

    public void setCornerAudioAvailable(String cornerAudioAvailable) {
        this.cornerAudioAvailable = cornerAudioAvailable;
    }

    public Object getCornerAudioBlueStreamUrl() {
        return cornerAudioBlueStreamUrl;
    }

    public void setCornerAudioBlueStreamUrl(Object cornerAudioBlueStreamUrl) {
        this.cornerAudioBlueStreamUrl = cornerAudioBlueStreamUrl;
    }

    public Object getCornerAudioRedStreamUrl() {
        return cornerAudioRedStreamUrl;
    }

    public void setCornerAudioRedStreamUrl(Object cornerAudioRedStreamUrl) {
        this.cornerAudioRedStreamUrl = cornerAudioRedStreamUrl;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Object getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(Object trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public String getArena() {
        return arena;
    }

    public void setArena(String arena) {
        this.arena = arena;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
