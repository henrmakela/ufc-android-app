package com.example.henri.ufcfighters;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.ContentValues.TAG;

public class MainActivity extends YouTubeBaseActivity{

    TextView fName;
    TextView fWeightClass;
    TextView fRecord;
    TextView fNickName;

    ImageView fImg;

    String searchName;
    Button search;
    Button events;
    Button highlightsBtn;
    Button titleHolders;




    YouTube youtube;
    private static final long MAX_RESULTS = 1;

    public String url= "http://ufc-data-api.ufc.com/api/v3/iphone/fighters";
    public static List<Fighter> fighterList = new ArrayList<>();
    Intent highlightIntent;
    Intent eventIntent;
    Intent titleHolderIntent;

    public ArrayList<Fighter> titleHolderNames;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        events = (Button)findViewById(R.id.eventsBtn);
        fName = (TextView)findViewById(R.id.fName);
        fWeightClass = (TextView)findViewById(R.id.fWeightclass);
        fRecord = (TextView)findViewById(R.id.fRecord);
        fImg = (ImageView)findViewById(R.id.fImage);
        highlightsBtn = (Button)findViewById(R.id.highlightsBtn);
        highlightsBtn.setEnabled(false);


        fNickName = (TextView)findViewById(R.id.fNickName);
        search = (Button)findViewById(R.id.search);


        titleHolders = (Button)findViewById(R.id.titleholdersBtn);


        final EditText name = (EditText) findViewById(R.id.name);


        highlightIntent = new Intent(this, HighlightsActivity.class);
        eventIntent = new Intent(this, EventsActivity.class);
        titleHolderIntent = new Intent(this,TitleHolders.class);


        try{
            run();
        }
        catch (IOException e){
            e.printStackTrace();
        }



        events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(eventIntent);
            }
        });

        titleHolders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                titleHolderNames = new ArrayList<Fighter>();

                for(Fighter f : fighterList){
                    if(f.isTitle_holder()){
                        titleHolderNames.add(f);
                    }
                }

                titleHolderIntent.putExtra("Title Holders", titleHolderNames);
                startActivity(titleHolderIntent);


            }
        });



        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager im = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                view.requestFocusFromTouch();

                String weightClass = "";

                searchName = name.getText().toString();
                if(searchName.isEmpty()){
                    Toast.makeText(MainActivity.this, "Type the name of a UFC fighter", Toast.LENGTH_SHORT).show();
                }
                else{
                    for(Fighter f : fighterList){



                        String fullName = f.getFirst_name() + " " + f.getLast_name();
                        if(fullName.toLowerCase().contains(searchName.toLowerCase())){

                            highlightsBtn.setEnabled(true);

                            if(f.getWeight_class().contains("_")){
                                String[] ar = f.getWeight_class().split("_");
                                weightClass = ar[0] + " " + ar[1];
                            }
                            else{
                                weightClass = f.getWeight_class();
                            }
                            fName.setText(f.getFirst_name() + " " + f.getLast_name());
                            fWeightClass.setText(weightClass);
                            fRecord.setText(f.getWins() + "-" + f.getLosses() + "-" + f.getDraws());
                            if(f.getNickname()!=null && f.getNickname() != ""){
                                fNickName.setText("'" + f.getNickname() + "'");
                            }
                            Glide.with(MainActivity.this).load(f.getLeft_full_body_image()).thumbnail(0.1f).into(fImg);


                        }


                    }
                }


            }
        });


       highlightsBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String searchWords = fName.getText().toString() + " highlights";

               new RetrieveVideoID().execute(searchWords);
           }
       });

        highlightsBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(MainActivity.this, "Click to watch highlights", Toast.LENGTH_LONG).show();
                return false;
            }
        });

    }

    void run() throws IOException {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String jsonData = response.body().string();

                Gson gson = new Gson();
                Type type = new TypeToken<List<Fighter>>() {}.getType();
                fighterList = gson.fromJson(jsonData, type);





                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {




                    }
                });

            }
        });
    }

    private class RetrieveVideoID extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {



            youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) throws IOException {

                }

            }).setApplicationName("com.example.henri.ufcfighters").build();


            String query = strings[0];

            Log.d(TAG, "doInBackground: " + query);
            try {
                YouTube.Search.List search = youtube.search().list("id,snippet");
                search.setKey(PlayerConfig.YOUTUBE_API_KEY);
                search.setQ(query);
                search.setType("video");
                search.setMaxResults(MAX_RESULTS);

                SearchListResponse searchResponse = search.execute();
                List<SearchResult> searchResultList = searchResponse.getItems();
                List<String> videoIds = new ArrayList<String>();


                for (SearchResult searchResult : searchResultList) {
                    videoIds.add(searchResult.getId().getVideoId());

                }

                return videoIds.get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }

        @Override
        protected void onPostExecute(String s) {
           super.onPostExecute(s);
            highlightIntent.putExtra("VideoId", s);
            startActivity(highlightIntent);



        }
    }


    }

