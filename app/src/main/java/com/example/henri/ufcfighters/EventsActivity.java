package com.example.henri.ufcfighters;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class EventsActivity extends AppCompatActivity {


    public String url= "http://ufc-data-api.ufc.com/api/v3/iphone/events";
    List<Event> eventList = new ArrayList<>();
    List<String>eventNames;
    ListView listView;
    Intent eventIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        eventNames = new ArrayList<>();
        eventIntent = new Intent(this, UpComingEventActivity.class);
        listView = (ListView)findViewById(R.id.eventsListView);
        try{
            run();

        }
        catch (IOException e){
            e.printStackTrace();
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {



            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String eventName = listView.getItemAtPosition(i).toString();

                for(Event e : eventList){
                    if(eventName.equals(e.getBaseTitle() + " " +e.getTitleTagLine())){
                        Event chosenEvent = e;
                        eventIntent.putExtra("Event", chosenEvent);
                    }
                    else if(eventName.equals(e.getBaseTitle() + " " + parseDate(e.getEventDate()))){
                        Event chosenEvent = e;
                        eventIntent.putExtra("Event", chosenEvent);
                    }


                }
                startActivity(eventIntent);


            }
        });



    }

    void run() throws IOException {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String jsonData = response.body().string();

                Gson gson = new Gson();
                Type type = new TypeToken<List<Event>>() {}.getType();
                eventList = gson.fromJson(jsonData, type);





                EventsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Date currentDate = Calendar.getInstance().getTime();



                        for(Event e : eventList){

                            int comparison = 0;
                            try {
                                comparison = currentDate.compareTo(toDate(parseDate(e.getEventDate())));
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }

                            if(comparison == -1){
                                if(e.getTitleTagLine() == null){
                                    eventNames.add(e.getBaseTitle() + " " + parseDate(e.getEventDate()));
                                }
                                else if(e.getTitleTagLine().equals("TBA vs TBD")){
                                    eventNames.add(e.getBaseTitle() + " " + parseDate(e.getEventDate()));
                                }
                                else{
                                    eventNames.add(e.getBaseTitle() + " " + e.getTitleTagLine());
                                }
                            }



                        }
                        populateListView();




                        /*for(Event e : eventList){


                                eventNames.add(e.getBaseTitle());


                                String[] dates = e.getEventDate().split("-");
                                String[] days = dates[2].split("T");
                                String dayOfTheMonthString = days[0];
                                String monthString = dates[1];

                                if(monthString.charAt(0)=='0'){
                                    monthString = monthString.substring(1);
                                }


                                if(dayOfTheMonthString.charAt(0)=='0'){
                                    dayOfTheMonthString = dayOfTheMonthString.substring(1);
                                }

                                int month = Integer.parseInt(monthString);
                                int dayOfTheMonth = Integer.parseInt(dayOfTheMonthString);
                                int year = Integer.parseInt(dates[0]);




                        }
                            */




                    }
                });

            }
        });
    }
    private void populateListView() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitems, eventNames);
        listView.setAdapter(adapter);
    }

    private String parseDate(String date){
        String newDate = "";

        String[] x = date.split("-");
        String[] dates = x[2].split("T");
        String dayOfTheMonthString = dates[0];
        String monthString = x[1];
        String years = x[0];

        newDate = dayOfTheMonthString + " " + monthString + " " + years;
        return  newDate;
    }

    private Date toDate(String dateString) throws ParseException {
        Date date = new SimpleDateFormat("dd MM yyyy").parse(dateString);
        return date;
    }
}
