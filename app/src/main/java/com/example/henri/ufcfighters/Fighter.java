package com.example.henri.ufcfighters;

import java.io.Serializable;

/**
 * Created by Henri on 14.7.2017.
 */

public class Fighter implements Serializable {


    private int id;
    private int wins;
    private int statid;
    private int losses;
    private String last_name;
    private String weight_class;
    private boolean title_holder;
    private int draws;
    private String first_name;
    private String fighter_status;
    private String thumbnail;
    private String nickname;
    private String rank;
    private Integer pound_for_pound_rank;
    private String belt_thumbnail;
    private String left_full_body_image;
    private String right_full_body_image;
    private String profile_image;
    private String link;


    public String getNickname() {
        return nickname;
    }

    public String getRank() {
        return rank;
    }

    public Integer getPound_for_pound_rank() {
        return pound_for_pound_rank;
    }

    public String getBelt_thumbnail() {
        return belt_thumbnail;
    }

    public String getLeft_full_body_image() {
        return left_full_body_image;
    }

    public String getRight_full_body_image() {
        return right_full_body_image;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getLink() {
        return link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getStatid() {
        return statid;
    }

    public void setStatid(int statid) {
        this.statid = statid;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getWeight_class() {
        return weight_class;
    }

    public void setWeight_class(String weight_class) {
        this.weight_class = weight_class;
    }

    public boolean isTitle_holder() {
        return title_holder;
    }

    public void setTitle_holder(boolean title_holder) {
        this.title_holder = title_holder;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getFighter_status() {
        return fighter_status;
    }

    public void setFighter_status(String fighter_status) {
        this.fighter_status = fighter_status;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
