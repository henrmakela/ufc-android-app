package com.example.henri.ufcfighters;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class UpComingEventActivity extends AppCompatActivity {


    TextView eventTitle;
    TextView eventArena;
    TextView titleTagLine;
    TextView eventDate;
    ImageView featuredImg;
    ImageView fighter1Img;
    ImageView fighter2Img;

    Fighter mainEventFighter1;
    Fighter mainEventFighter2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_coming_event);

        Intent i = getIntent();

        Event e = (Event)i.getSerializableExtra("Event");
        eventTitle = (TextView)findViewById(R.id.eventBaseTitle);
        eventArena = (TextView) findViewById(R.id.eventArena);
        eventDate = (TextView)findViewById(R.id.eventDate);

        titleTagLine = (TextView) findViewById(R.id.titleTagLine);


        fighter1Img = (ImageView)findViewById(R.id.fighter1imgV);
        fighter2Img = (ImageView)findViewById(R.id.fighter2imgV);


        for(Fighter f : MainActivity.fighterList){
            if(f.getId() == e.getMain_event_fighter1_id()){
                mainEventFighter1 = f;
            }
        }

        for(Fighter f : MainActivity.fighterList){
            if(f.getId() == e.getMain_event_fighter2_id()){
                mainEventFighter2 = f;
            }
        }

        if(e.getFeatureImage() != null){
            featuredImg = (ImageView)findViewById(R.id.featuredImg);
            Glide.with(UpComingEventActivity.this).load(e.getFeatureImage()).thumbnail(0.1f).into(featuredImg);
        }



        eventTitle.setText(e.getBaseTitle());
        eventArena.setText(e.getArena());
        eventDate.setText(parseDate(e.getEventDate()));
        if(e.getTitleTagLine() != null){
            titleTagLine.setText(e.getTitleTagLine());
        }
        else {
            titleTagLine.setVisibility(View.GONE);
        }

        if(mainEventFighter1 != null && mainEventFighter2 != null){

            Glide.with(UpComingEventActivity.this).load(mainEventFighter1.getProfile_image()).thumbnail(0.1f).into(fighter1Img);
            Glide.with(UpComingEventActivity.this).load(mainEventFighter2.getProfile_image()).thumbnail(0.1f).into(fighter2Img);
        }





    }
    private String parseDate(String date){
        String newDate = "";

        String[] x = date.split("-");
        String[] dates = x[2].split("T");
        String dayOfTheMonthString = dates[0];
        String monthString = x[1];
        String years = x[0];

        newDate = dayOfTheMonthString + " / " + monthString + " / " + years;
        return  newDate;
    }
}
