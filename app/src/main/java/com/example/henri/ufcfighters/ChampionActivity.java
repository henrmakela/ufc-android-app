package com.example.henri.ufcfighters;

import android.content.Intent;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ChampionActivity extends YouTubeBaseActivity {

    YouTubePlayerFragment youTubePlayerFragment;
    Intent hlIntent;
    YouTubePlayerView youTubePlayerView;
    YouTube youtube;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    String videoID;
    Button highlightsBtn;
    private static final long MAX_RESULTS = 1;

    TextView champName;
    TextView champWeightClass;
    TextView champRecord;
    TextView champNickName;
    TextView p4pRank;
    ImageView champImg;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_champion);

        champName = (TextView)findViewById(R.id.champName);
        champWeightClass = (TextView)findViewById(R.id.champWeightClass);
        champRecord = (TextView)findViewById(R.id.champRecord);
        champImg = (ImageView)findViewById(R.id.champImg);
        champNickName = (TextView) findViewById(R.id.chamNickname);
        p4pRank = (TextView) findViewById(R.id.p4pRank);
        /*youTubePlayerView = (YouTubePlayerView)findViewById(R.id.youtube_view2);*/
        highlightsBtn = (Button)findViewById(R.id.highlightsBtn);

        Intent i = getIntent();
        hlIntent = new Intent(this,HighlightsActivity.class);

        Fighter champ = (Fighter)i.getSerializableExtra("Champion");

        champName.setText(champ.getFirst_name() + " " + champ.getLast_name());
        champWeightClass.setText(champ.getWeight_class());
        champRecord.setText(champ.getWins() + " - " + champ.getLosses() + " - " + champ.getDraws());
        champNickName.setText(champ.getNickname());
        if(champ.getPound_for_pound_rank() !=null){
            p4pRank.setText("P4P: " + champ.getPound_for_pound_rank());
        }
        Glide.with(ChampionActivity.this).load(champ.getLeft_full_body_image()).thumbnail(0.1f).into(champImg);

        String searchWords = champ.getFirst_name() + " " + champ.getLast_name() + " highlights";
        new RetrieveVideoID().execute(searchWords);

        highlightsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hlIntent.putExtra("VideoId", videoID);
                startActivity(hlIntent);
            }
        });


    }

    private class RetrieveVideoID extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {



            youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) throws IOException {

                }

            }).setApplicationName("com.example.henri.ufcfighters").build();


            String query = strings[0];

            Log.d(TAG, "doInBackground: " + query);
            try {
                YouTube.Search.List search = youtube.search().list("id,snippet");
                search.setKey(PlayerConfig.YOUTUBE_API_KEY);
                search.setQ(query);
                search.setType("video");
                search.setMaxResults(MAX_RESULTS);

                SearchListResponse searchResponse = search.execute();
                List<SearchResult> searchResultList = searchResponse.getItems();
                List<String> videoIds = new ArrayList<String>();


                for (SearchResult searchResult : searchResultList) {
                    videoIds.add(searchResult.getId().getVideoId());

                }

                return videoIds.get(0);
            } catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            videoID = s;

          /* onInitializedListener = new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

                    youTubePlayer.cueVideo(videoID);


                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                    Toast.makeText(ChampionActivity.this, "Could not initialize the video", Toast.LENGTH_SHORT).show();
                }
            };

            youTubePlayerView.initialize(PlayerConfig.YOUTUBE_API_KEY, onInitializedListener);*/
        }
    }
}
