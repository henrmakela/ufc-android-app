package com.example.henri.ufcfighters;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class TitleHolders extends AppCompatActivity {

    ArrayList<Fighter> fightersList;
    ArrayList<String> fighterNames;
    ListView listView;
    Intent champIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_holders);

        fighterNames = new ArrayList<String>();

        Intent i = getIntent();
        champIntent = new Intent(this, ChampionActivity.class);

        fightersList = (ArrayList<Fighter>)i.getSerializableExtra("Title Holders");

        for(Fighter f : fightersList){
            fighterNames.add(f.getFirst_name() + " " + f.getLast_name());
        }



        populateListView();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String champName = listView.getItemAtPosition(i).toString();

                for(Fighter f : fightersList){
                    if(champName.contains(f.getFirst_name())){
                        Fighter champion = f;
                        champIntent.putExtra("Champion", champion);



                    }
                }
                startActivity(champIntent);


            }
        });

    }

    private void populateListView() {
        listView = (ListView)findViewById(R.id.titleHoldersList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.listitems, fighterNames);


        listView.setAdapter(adapter);
    }
}
